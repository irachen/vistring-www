$(".contact-form").submit(function submitFunc(event) {
  event.preventDefault();
  var form = $(this);
  var data = new FormData(form[0]);
  data.append("to", "info");
  data.append("last_name", "");
  $(".contact-submit-btn").attr("disabled", true);
  $.ajax({
    type: "POST",
    data: data,
    processData: false,
    contentType: false,
    url: form.attr("data-endpoint") + "/api/mails/contactus",
    success: function () {
      console.log("success");
      form[0].reset();
      $(".contact-submit-btn").attr("disabled", false);
      $(".contact-sent-fail").hide();
      $(".contact-sent-succ").show();
    },
    fail: function () {
      console.log("fail");
      $(".contact-submit-btn").attr("disabled", false);
      $(".contact-sent-succ").hide();
      $(".contact-sent-fail").show();
    },
  });
});

$(document).ready(function () {
  $("#particles").particleground({
    dotColor: "#5cbdaa",
    lineColor: "#5cbdaa",
  });
});

for (var i = 0; i < 4; i++) {
  $("#item-" + i).mouseenter(autoExpand);
}

function autoExpand() {
  $(".auto-expend-block").removeClass("active-block");
  $(this).addClass("active-block");
}
