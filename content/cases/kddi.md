---
title: "携手日本KDDI，实现“麒麟挑战杯足球赛” 8K VR云直播"
description: "视见睿来助力KDDI在日本为全国足球比赛 “麒麟挑战杯”进行5G+8K VR直播。"
weight: 4
date: 2020-07-01T11:56:10+08:00

tags: ["5G+VR", "8K", "VR直播", "体育赛事"]
desc: "2019年6月，视见睿来成功助力KDDI在日本为 “麒麟挑战杯” 全国足球赛进行8K VR直播。团队主要负责对8K VR视频流进行实时云转码，并主导了项目技术部分的策划、联调与落地。直播活动获得巨大成功，现场画面经过采集、转码、分发后最终通过安装了视见睿来8K VR播放器的头显播放，画面清晰流畅，赢得主办方与球迷朋友们的一致好评。"
keyname: "kddi"
location: "日本"
industry: "体育赛事"
website: "kddi.com"
websiteLink: "https://www.kddi.com"
product: "一站式VR直播解决方案"

productKey: "live"

customer: "KDDI是日本第三大电信服务提供商，提供的业务包括固定业务和移动业务。在日本，KDDI以其超过5000万用户的移动服务（ “au”）而闻名。KDDI不断探索5G新应用，希望为客户带来多样化服务。"
project: "赛事VR直播是5G应用中最具发展潜力的落地方向。2019年6月，由日本足球协会主办的2019年全国足球比赛 “麒麟挑战杯”在日本宫城体育场（Hitomebore Stadium Miyagi ）开赛，KDDI希望可以借此机会探索5G商用新应用——为其进行8K VR直播，给粉丝们带去身临其境的沉浸式观看体验。KDDI经过严格的供应商筛选和谨慎测试后选择与视见睿来合作。"
solution: ["视见睿来负责转码、分发与播放部分，网络部分则由KDDI负责；前端使用看到科技的360 8K 相机，终端头显采用 Pico G2 4K头显。活动当天，360 8K相机所采集的视频源经过现场缝合编码后以RTMP的视频格式传输到VR云，并通过全球独家VVOS技术进行实时云转码，在保持高清画质的同时节省50～80%的传输数据，完成转码后，视频流通过KDDI为活动专程提供的网络传输到头显上，最终由视见睿来提供的8K VR播放器播放。"]
result: ["比赛当天，数台Pico G2 4K头显放在体育场的入口的VR体验区，数千名球迷朋友们排长队希望尝鲜体验，通过“第一视角”与自己喜欢的球员们进行了亲密接触；赛事通过视见睿来与设备/技术供应商们的无缝合作得到顺利直播，画面清晰流畅，效果逼真，赢得球迷朋友们与主办方的高度称赞。"]

linkKeyname: "tata"
linkTitle: "携手英国塔塔通信，实现“欧巡赛”全球首次8K VR云直播"

productTitle: "一站式VR云直播"
productBackground: "/images/home_bg_live.jpg"
productContent: "一站式解决方案助您轻松实现跨平台超高清VR直播，为观众带去非凡体验。"
productUrl: "/live_vr"
productKey: "live"

---
